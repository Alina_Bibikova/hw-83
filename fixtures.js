const mongoose = require('mongoose');
const config = require('./config');

const Artist = require('./models/Artist');
const Albums = require('./models/Albums');
const Tracks = require('./models/Tracks');

const run = async () => {

    await mongoose.connect(config.dbUrl, config.mongoOptions);

    const connection = mongoose.connection;

    const collections = await connection.db.collections();

    for (let collection of collections) {
        await collection.drop();
    }

    const [johny, adele, lana] = await Artist.create(
        {   name: 'Johnny Cash',
            description: 'Hurt',
            image: 'johnnyCash.jpg'

        },
        {
            name: 'Adele',
            description: 'Someone Like You',
            image: 'adel.jpeg'
        },
        {
            name: 'Lana Del Rey',
            description: 'Ride',
            image: 'lana.jpg'
        },
    );

    const [johnyAlb, adeleAlb, lanaAlb] =  await Albums.create(
        {   nameAlbums: 'The Man Comes Around',
            nameArtist: johny._id,
            date: 2002,
            image: 'cash.jpg'
        },

        {   nameAlbums: 'Adele 21',
            nameArtist: adele._id,
            date: 2011,
            image: 'adele.jpg'
        },

        {   nameAlbums: 'Born to Die',
            nameArtist: lana._id,
            date: 2012,
            image: 'born.jpg'
        },
    );

    await Tracks.create(
        {   nameTrack: 'Hurt',
            album: johnyAlb._id,
            longest: 2002,
        },

        {   nameTrack: 'Someone Like You',
            album: adeleAlb._id,
            longest: 2011,
        },

        {   nameTrack: 'Born to Die',
            album: lanaAlb._id,
            longest: 2012,
        },
    );

    await connection.close();
};

run().catch(error => {
    console.error('Something went wrong', error);
});