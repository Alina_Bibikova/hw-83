const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const TracksSchema = new Schema({
    nameTrack: {
        type: String, required: true
    },
    album: {
        type: Schema.ObjectId,
        ref: 'Albums'
    },
    longest: String
});

const Tracks = mongoose.model('Tracks', TracksSchema);

module.exports = Tracks;