const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const TrackHistorySchema = new Schema({
    user: {
        type: Schema.ObjectId,
        ref: 'User'
    },
    track: {
        type: Schema.ObjectId,
        ref: 'Tracks'
    },
    datetime: {
        type: String,
        unique: true
    }
});

const TrackHistory = mongoose.model('TrackHistory', TrackHistorySchema);

module.exports = TrackHistory;