const express = require('express');
const mongoose = require('mongoose');

const artist = require('./app/artist');
const albums = require('./app/albums');
const tracks = require('./app/tracks');
const user = require('./app/user');
const trackHistory = require('./app/trackHistory');

const config = require('./config');

const cors = require('cors');
const app = express();

app.use(express.json());
app.use(express.static('public'));
app.use(cors());

const port = 8000;

mongoose.connect(config.dbUrl, config.mongoOptions).then( () => {

    app.use('/artist', artist);
    app.use('/albums', albums);
    app.use('/tracks', tracks);
    app.use('/user', user);
    app.use('/track_history', trackHistory)
});

app.listen(port, () => {
    console.log(`Server started on ${port} port!`);
});


