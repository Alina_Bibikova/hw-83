const express = require('express');
const TrackHistory = require('../models/TrackHistory');
const User = require('../models/User');

const router = express.Router();

router.post('/', async (req, res) => {
    const token = req.get('Token');
    const user = await User.findOne({token: token});
    if (!user) {
        return res.status(401).send({error: 'Unauthorized'});
    }


    const trackHistory = await new TrackHistory({
        user: user._id,
        track: req.body.track,
        datetime: new Date().toISOString()
    });

    await trackHistory.save();
    res.send(trackHistory);

});

module.exports = router;