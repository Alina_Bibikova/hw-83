const express = require('express');
const Tracks = require('../models/Tracks');

const router = express.Router();

router.get('/', (req, res) => {
    if (req.query.album) {
        Tracks.findOne({album: req.query.album}).populate('album').then(result => {
            res.send(result);
        })
    } else {
        Tracks.find().populate('album')
            .then(track => res.send(track))
            .catch(() => res.sendStatus(500));
    }
});

module.exports = router;