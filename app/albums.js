const express = require('express');
const multer = require('multer');
const path = require('path');
const config = require('../config');
const nanoid = require('nanoid');
const Albums = require('../models/Albums');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname))
    }
});

const upload = multer({storage});

const router = express.Router();

router.get('/', (req, res) => {
    if(req.query.artist) {
        Albums.findOne({nameArtist: req.query.artist}).populate('nameArtist').then(result => {
            res.send(result)
        })
    } else {
        Albums.find().populate('nameArtist')
            .then(albums => {
                res.send(albums)
            })
            .catch(() => res.sendStatus(500));
    }
});

router.get('/:id', (req, res) => {
    Albums.findById(req.params.id).populate('nameArtist')
        .then(result => {
            if (result) return res.send(result);
            res.sendStatus(404);
        })
        .catch(() => res.sendStatus(500));
});

router.post('/', upload.single('image'), (req, res) => {
    const AlbumData = req.body;
    if (req.file) {
        AlbumData.image = req.file.filename;
    }
    const albums = new Albums(AlbumData);

    albums.save()
        .then(result => res.send(result))
        .catch(error => res.status(400).send(error))
});

module.exports = router;